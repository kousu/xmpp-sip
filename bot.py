#!/usr/bin/env python3

import os
import logging

import asyncio
from aioconsole import ainput

from slixmpp import ClientXMPP
from slixmpp import JID
from slixmpp.exceptions import IqError, IqTimeout
from slixmpp.xmlstream.matcher import MatchXPath
from slixmpp.xmlstream.handler import Callback
from slixmpp.xmlstream import StanzaBase
from slixmpp.stanza import Message, Iq
from slixmpp.xmlstream import register_stanza_plugin

from slixmpp.plugins import BasePlugin

class XEP_0353(BasePlugin):

    """
    XEP-0353: Jingle Message Initiation

    Provides a way to do multi-client ringing.

    Also see <http://www.xmpp.org/extensions/xep-0353.html>.

    """
    name = 'xep_0353'
    description = 'XEP-0353: Jingle Message Initiation'
    dependencies = set()
    #stanza = stanza
    # TODO

class Propose(StanzaBase):
    name = 'propose'
    namespace = 'urn:xmpp:jingle-message:0'
    plugin_attrib = name

class Retract(StanzaBase):
    name = 'retract'
    namespace = 'urn:xmpp:jingle-message:0'
    plugin_attrib = name

class Description(StanzaBase):
    name = 'description'
    namespace = 'urn:xmpp:jingle:apps:rtp:1'
    interfaces = {'media'}
    plugin_attrib = name

register_stanza_plugin(Propose, Description)

class Accept(StanzaBase):
    name = 'accept'
    namespace = 'urn:xmpp:jingle-message:0'
    plugin_attrib = name

class Reject(StanzaBase):
    name = 'reject'
    namespace = 'urn:xmpp:jingle-message:0'
    plugin_attrib = name

class Proceed(StanzaBase):
    name = 'proceed'
    namespace = 'urn:xmpp:jingle-message:0'
    plugin_attrib = name

register_stanza_plugin(Message, Propose)
register_stanza_plugin(Message, Retract)
register_stanza_plugin(Message, Accept)
register_stanza_plugin(Message, Proceed)
register_stanza_plugin(Message, Reject)

class XEP_0166(BasePlugin):

    """
    XEP-0166: Jingle

    Signalling (i.e. connection metadata) for making out-of-XMPP peer-to-peer connections,
    meant mostly for voice calling but also used for file transfers.
    Covers opening and closing connections and negotiating formats.

    Also see <https://xmpp.org/extensions/xep-0166.html>.
    Also also see <https://xmpp.org/registrar/jingle-transports.html>.

    """
    name = 'xep_0166'
    description = 'XEP-0166: Jingle'
    dependencies = set()
    #stanza = stanza
    # TODO

class Jingle(StanzaBase):
    name = 'jingle'
    namespace = 'urn:xmpp:jingle:1'
    plugin_attrib = name
    interfaces = {'action','initiator','sid'}
    sub_interfaces = {'content','reason'}

class Group(StanzaBase):
    name = 'group'
    namespace = "urn:xmpp:jingle:apps:grouping:0"
    interfaces = {'semantics'}
    plugin_attrib = name

register_stanza_plugin(Jingle, Group)

class GroupContent(StanzaBase):
    name = 'content'
    namespace = Group.namespace
    interfaces = {'name'}
    plugin_multi_attrib = name

register_stanza_plugin(Group, GroupContent, iterable=True)

class Content(StanzaBase):
    name = 'content'
    namespace = Jingle.namespace
    plugin_multi_attrib = name
    interfaces = {'creator', 'name', 'senders', 'disposition'}
    #creator_types = {'initiator','responder'}
    #senders_types = {'both', 'initiator', 'none', 'responder'}

class ICEUDPTransport(StanzaBase):
    name = 'transport'
    namespace = 'urn:xmpp:jingle:transports:ice-udp:1'
    interfaces = {'pwd', 'ufrag'}
    plugin_attrib = name
    sub_interfaces = {'fingerprint'}

class PayloadType(StanzaBase):
    name = 'payload-type'
    namespace = Description.namespace
    plugin_multi_attrib = name
    interfaces = {'name', 'clockrate', 'id', 'channels'}

class ICEUDPTransportCandidate(StanzaBase):
    name = 'candidate'
    namespace = ICEUDPTransport.namespace
    plugin_multi_attrib = name
    interfaces = {'component','foundation','generation','id','ip','network','port','priority','protocol', 'rel-addr', 'rel-port', 'type'}
    protocol_types = {'udp', 'tcp'}
    types = {'relay', # TURN
                  'srflx', # STUN
                  'host',  # direct connection
                  }

class DTLSFingerprint(StanzaBase):
    name = 'fingerprint'
    namespace = 'urn:xmpp:jingle:apps:dtls:0'
    interfaces = {'setup', 'hash'}
    plugin_attrib = name
    # ??? how do I get the actual fingerprint out??

register_stanza_plugin(Iq, Jingle)
register_stanza_plugin(Jingle, Content, iterable=True)
register_stanza_plugin(Content, Description)
register_stanza_plugin(Content, ICEUDPTransport)
register_stanza_plugin(ICEUDPTransport, ICEUDPTransportCandidate, iterable=True)
register_stanza_plugin(Description, PayloadType, iterable=True)
register_stanza_plugin(ICEUDPTransport, DTLSFingerprint)

class PayloadTypeParameter(StanzaBase):
    name = 'parameter'
    namespace = PayloadType.namespace
    interfaces = {'name', 'value'}
    plugin_attrib = name

class RTC_FB(StanzaBase):
    name = 'rtcp-fb'
    namespace = "urn:xmpp:jingle:apps:rtp:rtcp-fb:0"
    types = {'transport-cc'}
    plugin_attrib = name

register_stanza_plugin(PayloadType, PayloadTypeParameter)
register_stanza_plugin(PayloadType, RTC_FB)

class RTCP_MUX(StanzaBase):
    name = 'rtcp-mux'
    namespace = Description.namespace
    plugin_attrib = name

register_stanza_plugin(Description, RTCP_MUX)

class RTP_HDREXT(StanzaBase):
    name = 'rtp-hdrext'
    namespace = "urn:xmpp:jingle:apps:rtp:rtp-hdrext:0"
    interfaces = {'id', 'uri'}
    plugin_multi_attrib = name

register_stanza_plugin(Description, RTP_HDREXT, iterable=True)

class Source(StanzaBase):
    name = 'source'
    namespace = "urn:xmpp:jingle:apps:rtp:ssma:0"
    interfaces = {'ssrc'}
    plugin_attrib = name

class SourceParameter(StanzaBase):
    name = 'parameter'
    namespace = Source.namespace
    interfaces = {'name', 'value'}
    plugin_multi_attrib = name

register_stanza_plugin(Source, SourceParameter, iterable=True)


class VoIPBot(ClientXMPP):

    def __init__(self, jid, password):
        ClientXMPP.__init__(self, jid, password)

        self.add_event_handler("session_start", self.session_start)
        self.add_event_handler("message", self.message)
        self.add_event_handler("ring", self.ring)

        self.add_event_handler("session_initiate", self.jingle_session_initiate)

        # TODO: pull this out to self.register_plugin('xep_0353')
        # ...is there a way to just say "register Description"? Because the Stanzas all know their expected place in the hierarchy..mostly?
        self.register_handler(
            Callback('ring',
                     MatchXPath('{%s}message/{%s}propose/{%s}description' %
                                 (self.default_ns, 'urn:xmpp:jingle-message:0', 'urn:xmpp:jingle:apps:rtp:1')),
                     self._handle_ring))

        self.register_handler(
            Callback('jingle',
                       MatchXPath('{%s}iq/{%s}jingle' % (self.default_ns, 'urn:xmpp:jingle:1')),
                       self._handle_jingle))

        self.register_plugin('xep_0030') # Service Discovery
        self.register_plugin('xep_0115') # Entity Capabilities
        self.register_plugin('xep_0199') # XMPP Ping

        #self.register_plugin('xep_0166') # Jingle
        #self.register_plugin('xep_0176') # ICE UDP
        #self.register_plugin('xep_0167') # RTP
        #self.register_plugin('xep_0353') # Jingle Message Initiation (i.e. ring multiple devices at once)

    async def call(self, jid):

        proposal = self.Message()
        proposal['type'] = 'chat'
        proposal['from'] = self.boundjid.full
        proposal['to'] = jid
        p = Propose()
        session = p['id'] = self.new_id()
        d = Description()
        d['media'] = 'audio'
        p.append(d)
        proposal.append(p)
        proposal.send() # ... ?:q

        self.send(proposal)
        self.send(Iq())
        # DEBUG    SEND: <message id="11d392afe8684bc1b1b00de51260a9ed" xml:lang="en" type="chat" from="callme@kousu.ca/Puh22U9U" to="vagabond@kousu.ca"><origin-id xmlns="urn:xmpp:sid:0" id="11d392afe8684bc1b1b00de51260a9ed" /><propose xmlns="urn:xmpp:jingle-message:0" id="1bed40b4e129497281cb314645981da9"><description xmlns="urn:xmpp:jingle:apps:rtp:1" media="audio" /></propose></message>


        # TODO: write this all as coroutines
        # conversations has an explicit State machine with a transition() function
        # I think you can do the same but all in one place with a coroutine, something that can take inputs and outputs
        # I can hook it all together with event handlers that just catch e.g. f"{Iq.namespace}iq/{Jingle.namespace}jingle[@sid='{session}']" 
        # (is it important for timeouts to be involved too?)

        # now I await for
        #DEBUG    RECV: <message from="vagabond@kousu.ca/Conv6ations.14bu" type="chat" to="callme@kousu.ca/Puh22U9U" id="jm-proceed-1bed40b4e129497281cb314645981da9"><proceed xmlns="urn:xmpp:jingle-message:0" id="1bed40b4e129497281cb314645981da9" /><store xmlns="urn:xmpp:hints" /><stanza-id xmlns="urn:xmpp:sid:0" by="callme@kousu.ca" id="lkxoXLd5r9p3AT2H" /></message>
        async def send_session_initiate(proceed):
            proceed.reply().send()

            # ugh can't i do this with awaits instead of a temporary inner handler somehow?
            self.del_event_handler(f'proceed-{session}', send_session_initiate) # layer of indirection so that async works
            self.remove_handler('proceed-'+session)

            # now hand-off to Jingle; send a session-initiate

            init = self.Iq()
            init['type'] = 'set'
            init['from'] = self.boundjid.full
            init['to'] = proceed['from']

            jingle = Jingle()
            jingle['action'] = 'session-initiate'
            #jingle['initiator'] = self.boundjid.full # redundant and unused by Conversations
            jingle['sid'] = session
            init.append(jingle)

            group = Group()
            group['semantics'] = 'BUNDLE'
            content = GroupContent()
            content['name'] = 'audio'
            group.append(content)
            jingle.append(group)

            content = Content()
            content['creator'] = 'initiator'
            content['name'] = 'audio'
            description = Description()
            description['media'] = 'audio'

            payloadtype = PayloadType()
            payloadtype['id'] = '111'
            payloadtype['name'] = 'opus'
            payloadtype['clockrate'] = '48000'
            payloadtype['channels'] = '2'
            parameter = PayloadTypeParameter()
            parameter['name'] = 'minptime'
            parameter['value'] = '10'
            payloadtype.append(parameter)
            parameter = PayloadTypeParameter()
            parameter['name'] = 'useinbandfec'
            parameter['value'] = '1'
            payloadtype.append(parameter)
            rtcfb = RTC_FB()
            rtcfb['type'] = 'transport-cc'
            payloadtype.append(rtcfb)
            description.append(payloadtype)

            payloadtype = PayloadType()
            payloadtype['id'] = '103'
            payloadtype['name'] = 'ISAC'
            payloadtype['clockrate'] = '16000'
            description.append(payloadtype)

            payloadtype = PayloadType()
            payloadtype['id'] = '9'
            payloadtype['name'] = 'G722'
            payloadtype['clockrate'] = '8000'
            description.append(payloadtype)

            payloadtype = PayloadType()
            payloadtype['id'] = '102'
            payloadtype['name'] = 'ILBC'
            payloadtype['clockrate'] = '8000'
            description.append(payloadtype)

            payloadtype = PayloadType()
            payloadtype['id'] = '0'
            payloadtype['name'] = 'PCMU'
            payloadtype['clockrate'] = '8000'
            description.append(payloadtype)

            payloadtype = PayloadType()
            payloadtype['id'] = '8'
            payloadtype['name'] = 'PCMA'
            payloadtype['clockrate'] = '8000'
            description.append(payloadtype)

            payloadtype = PayloadType()
            payloadtype['id'] = '105'
            payloadtype['name'] = 'CN'
            payloadtype['clockrate'] = '16000'
            description.append(payloadtype)

            payloadtype = PayloadType()
            payloadtype['id'] = '13'
            payloadtype['name'] = 'CN'
            payloadtype['clockrate'] = '8000'
            description.append(payloadtype)

            payloadtype = PayloadType()
            payloadtype['id'] = '110'
            payloadtype['name'] = 'telephone-event'
            payloadtype['clockrate'] = '48000'
            description.append(payloadtype)

            payloadtype = PayloadType()
            payloadtype['id'] = '113'
            payloadtype['name'] = 'telephone-event'
            payloadtype['clockrate'] = '16000'
            description.append(payloadtype)

            payloadtype = PayloadType()
            payloadtype['id'] = '126'
            payloadtype['name'] = 'telephone-event'
            payloadtype['clockrate'] = '8000'
            description.append(payloadtype)

            hdrext = RTP_HDREXT()
            hdrext['id'] = '1'
            hdrext['uri'] = 'urn:ietf:params:rtp-hdrext:ssrc-audio-level'
            description.append(hdrext)

            hdrext = RTP_HDREXT()
            hdrext['id'] = '2'
            hdrext['uri'] = 'http://www.webrtc.org/experiments/rtp-hdrext/abs-send-time'
            description.append(hdrext)

            hdrext = RTP_HDREXT()
            hdrext['id'] = '3'
            hdrext['uri'] = 'http://www.ietf.org/id/draft-holmer-rmcat-transport-wide-cc-extensions-01'
            description.append(hdrext)

            source = Source()
            source['ssrc'] = '2171595443'
            parameter = SourceParameter()
            parameter['name'] = 'cname'
            parameter['value'] = 'MiwImm6XIkJMioPZ'
            source.append(parameter)
            parameter = SourceParameter()
            parameter['name'] = 'msid'
            parameter['value'] = 'my-media-stream my-audio-track'
            source.append(parameter)
            parameter = SourceParameter()
            parameter['name'] = 'mslabel'
            parameter['value'] = 'my-media-stream'
            source.append(parameter)
            parameter = SourceParameter()
            parameter['name'] = 'label'
            parameter['value'] = 'my-audio-track'
            source.append(parameter)

            description.append(source)

            rtcp_mux = RTCP_MUX()
            description.append(rtcp_mux)

            content.append(description)

            transport = ICEUDPTransport()
            transport['pwd'] = 'asd88fgpdd777uzjYhagZg'
            transport['ufrag'] = '8hhy'
            candidate = ICEUDPTransportCandidate()
            candidate['component'] = '1'
            candidate['foundation'] = '1'
            candidate['generation'] = '0'
            candidate['ip'] = '192.168.7.191' # TODO: don't hardcode!
            candidate['network'] = '0'
            candidate['port'] = '3478'
            candidate['priority']='2130706431'
            candidate['protocol'] = 'udp'
            candidate['type'] = 'host'
            transport.append(candidate)
            fingerprint = DTLSFingerprint()
            fingerprint['hash'] = 'sha-256'
            fingerprint['setup'] = 'actpass'
            fingerprint.xml.text = 'C7:E5:42:0F:BE:F5:B0:3F:4E:44:AB:75:D1:B4:99:86:91:65:EB:55:E4:DE:32:BF:19:F6:40:2D:E6:5A:4E:BF'
            transport.append(fingerprint)

            content.append(transport)

            jingle.append(content)

            print("---------------------")
            print(init)
            print("---------------------")
            import time; time.sleep(5)
            init.send()

        self.add_event_handler(f'proceed-{session}', send_session_initiate) # layer of indirection so that async works
        self.register_handler(
            Callback(f'proceed-{session}',
                     MatchXPath(f"{{{Message.namespace}}}message/{{{Proceed.namespace}}}proceed[@id='{session}']"),
                     lambda stanza: self.event(f'proceed-{session}', stanza)
                     ))

    def _handle_ring(self, stanza):
        self.event('ring', stanza)  # hmm. register_handler()'s callbacks can't be coroutines, so we need this layer of indirection.
        # TODO: write this as self['xep_0353'].add_ring_handler(self.ring)?
        # and also most of the code in that can be pulled out; the only things we need to expose are what happens if the other end -> rejects, or we answer

    def _handle_jingle(self, stanza):
        if stanza['jingle']['action'] == 'session-initiate':
            self.event('session_initiate', stanza)
        elif stanza['jingle']['action'] == 'session-terminate':
            self.event('session_terminate', stanza)

    async def jingle_session_initiate(self, stanza):

        # https://xmpp.org/extensions/xep-0176.html#protocol-response
        # > As described in XEP-0166, to acknowledge receipt of the session initiation request, the responder immediately returns an IQ-result.
        stanza.reply().send()

        # Construct a https://xmpp.org/extensions/xep-0166.html#def-action-session-accept
        accept = Iq(self)
        accept['type'] = 'set'
        accept['from'] = stanza['to']
        accept['to'] = stanza['from']
        jingle = Jingle()
        accept.append(jingle)
        jingle['action'] = 'session-accept' # stanza['jingle']['action']
        jingle['responder'] = stanza['to']
        jingle['sid'] = stanza['jingle']['sid']

        for ctnt in stanza['jingle']['content']:
            content = Content()
            jingle.append(content)
            content['creator'] = ctnt['creator']
            content['name'] = ctnt['name']

            description = Description()
            #content.append(description)
            content.append(ctnt['description'])

            desc = ctnt['description']
            description['media'] = desc['media']
            for fmt in ctnt['description']['payload-type']:
                # if formats are something we can handle ..
                description.append(fmt)
                # break # just take the first one

            # oh boy this is confusing
            # https://xmpp.org/extensions/xep-0166.html#def-action-session-initiate shows an example with a single transport containing multiple candidate endpoints
            # but conversations sends a single transport with only a *fingerprint* in it, followed up by a separate transport-infos
            # ...hm.
            transport = ctnt['transport'] # XXX this is definitely wrong; we need to fill in our own DTLS key here
            #transport = ICEUDPTransport()
            transport['fingerprint']['setup'] = 'active' # or 'passive' -- basically who connects to whom
            candidate = ICEUDPTransportCandidate()
            candidate['component'] = '1'
            candidate['foundation'] = '1'
            candidate['generation'] = '0'
            candidate['ip'] = '192.168.7.191' # TODO: don't hardcode!
            candidate['network'] = '0'
            candidate['port'] = '3478'
            candidate['priority']='2130706431'
            candidate['protocol'] = 'udp'
            candidate['type'] = 'host'
            transport.append(candidate)
            content.append(transport)
            #tspt = ctnt['transport']
            #transport['pwd'] = tspt['pwd']
            #transport['ufrag'] = tspt['ufrag']
            #for cda in tspt['candidate']:
            #    # ???? try them one after the other??
            #    transport.append(cda)

        #import pdb; pdb.set_trace()
        accept.send()

    async def ring(self, stanza):
        """
        Accept (or reject) a call from XMPP.
        """

        # TODO: determine if it's audio or audio/video, and display that to the user
        #  - ack, a video call has *two* <description>s in it:
        #    <propose xmlns="urn:xmpp:jingle-message:0" id="IM6qGpZQNX8Tb0dGfGm4+g"><description xmlns="urn:xmpp:jingle:apps:rtp:1" media="audio" /><description xmlns="urn:xmpp:jingle:apps:rtp:1" media="video" /></propose>
        #    but stanza['propose']['description']['media'] only returns the last. what do?

        # TODO: pull this out to self.register_plugin('xep_0353')
        # ...is there a way to just say "register Description"? Because the Stanzas all know their expected place in the hierarchy..mostly?

        # catch <retract>ions of this particular call
        # (this seems..bad. is there a way to make this less bad?)
        retracted = False
        def retract(retract_stanza):
            nonlocal retracted
            if retract_stanza['retract']['id'] == stanza['propose']['id']:
                retracted = True
        self.register_handler(
            Callback(f"retract_{stanza['propose']['id']}",
                     MatchXPath('{%s}message/{%s}retract' %
                                 (self.default_ns, Retract.namespace)),
                     retract))

        resp = await ainput(f"Do you want to accept the call from {stanza['from']}? [y/N]")
        if resp.lower().strip() == 'y':
            answer = True
        else:
            answer = False
        answer = True

        # ugh ugh ugh
        # what we really want is a *cancellable* input()
        if retracted:
            self.remove_handler(f"retract_{stanza['propose']['id']}")
            return

        # this is a case for select(): I want to select() on [input(), <retract>]
        # but I don't know how to do that.

        if answer:
            # https://xmpp.org/extensions/xep-0353.html#accept
            # 1. Stop our other selves ringing
            #r = self.Message() # ?
            r = stanza.reply()
            r.append(Accept(sid=stanza['propose']['id']))
            r['from'] = self.boundjid.full
            r['to'] = self.boundjid.bare
            r.send()

            # 2. Answer the call
            r = stanza.reply()
            r.append(Proceed(sid=stanza['propose']['id']))
            r.send()

            # 3. (optional) Send presence, in case we were invisible or the partner just missed us signing on by accident
            r = self.Presence()
            r['from'] = stanza['to']
            r['to'] = stanza['from']
            r.send()

            # NB: there will be a fourth stanza in the logs here: the server will reflect our 'Accept' back to us.

            # We don't process any more here; instead, the other side will send us a <jingle type='session-initiate'> and we handle that in a separate handler.
        else:
            # 1. Tell our other devices we're hanging up
            r = stanza.reply()
            r.append(Reject(sid=stanza['propose']['id']))
            r['from'] = self.boundjid.full
            r['to'] = self.boundjid.bare
            r.send()

            # 2. Reject the call
            r = stanza.reply()
            r['type'] = None
            r.append(Reject(sid=stanza['propose']['id']))
            r.send()

        self.remove_handler(f"retract_{stanza['propose']['id']}") # XXX duplicated

    async def session_start(self, event):
        # once we're online, declare our identity
        # (it's important to be online first because in case
        #  of conflicting resources, the server decides)
        self['xep_0030'].add_identity(category='client', itype='transport', name='VoIP Bot')

        # https://gist.github.com/iNPUTmice/a28c438d9bbf3f4a3d4c663ffaa224d9#notes-for-developers: these 6 caps => VoIP
        self['xep_0030'].add_feature('urn:xmpp:jingle:1')
        self['xep_0030'].add_feature('urn:xmpp:jingle:transports:ice-udp:1')
        self['xep_0030'].add_feature('urn:xmpp:jingle:apps:rtp:1')
        self['xep_0030'].add_feature('urn:xmpp:jingle:apps:dtls:0')
        self['xep_0030'].add_feature('urn:xmpp:jingle:apps:rtp:audio')
        self['xep_0030'].add_feature('urn:xmpp:jingle:apps:rtp:video')

        # This is mandatory to get XEP-0030 to work:
        # https://xmpp.org/extensions/xep-0115.html
        # > Clients should not engage in the older "disco/version flood" behavior
        # > and instead should use Entity Capabilities as specified herein.
        await self['xep_0115'].update_caps()

        self.send_presence()
        self.get_roster()

        # TODO: should this be done in main(), after connect()? is that possible?
        if os.environ.get("TARGET",False):
            await self.call(os.environ["TARGET"])


    def message(self, msg):
        #import pdb; pdb.set_trace()
        if msg['type'] in ('chat', 'normal'):
            msg.reply("Thanks for sending\n%(body)s" % msg).send()


class JingleMITM(ClientXMPP):
    def __init__(self, jid, password, left, right):
        self.target_left, self.target_right = JID(left), JID(right)

        ClientXMPP.__init__(self, jid, password)

        self.register_handler(
            Callback('ring',
                     MatchXPath('{%s}message/{%s}' %
                                 (self.default_ns, 'urn:xmpp:jingle-message:0')),
                     self._proxy))

        self.register_handler(
            Callback('jingle',
                       MatchXPath('{%s}iq/{%s}jingle' % (self.default_ns, 'urn:xmpp:jingle:1')),
                       self._proxy))

        self.register_plugin('xep_0030') # Service Discovery
        self.register_plugin('xep_0115') # Entity Capabilities
        self.register_plugin('xep_0199') # XMPP Ping

        self.add_event_handler("session_start", self.session_start)

    async def session_start(self, event):
        self['xep_0030'].add_identity(category='client', itype='mobile', name='VoIP Bot')
        # https://gist.github.com/iNPUTmice/a28c438d9bbf3f4a3d4c663ffaa224d9#notes-for-developers: these 6 caps => VoIP
        self['xep_0030'].add_feature('urn:xmpp:jingle:1')
        self['xep_0030'].add_feature('urn:xmpp:jingle:transports:ice-udp:1')
        self['xep_0030'].add_feature('urn:xmpp:jingle:apps:rtp:1')
        self['xep_0030'].add_feature('urn:xmpp:jingle:apps:dtls:0')
        self['xep_0030'].add_feature('urn:xmpp:jingle:apps:rtp:audio')
        self['xep_0030'].add_feature('urn:xmpp:jingle:apps:rtp:video')
    
        # This is mandatory to get XEP-0030 to work:
        # https://xmpp.org/extensions/xep-0115.html
        # > Clients should not engage in the older "disco/version flood" behavior
        # > and instead should use Entity Capabilities as specified herein.
        await self['xep_0115'].update_caps()

        self.send_presence()
        self.get_roster()

    def _proxy(self, stanza):
        #import time
        #print("--------------_")
        #print()
        #print(f"_proxy({repr(stanza)})")
        #print()
        #print("--------------_")
        #import sys; sys.stdout.flush()
        #time.sleep(5) # beware: this glitches out Conversations, since it lets you accept a call on target_left before target_right realizes it knows about target_left's endpoints
        if stanza['from'].bare == self.target_left.bare:
            target = self.target_right
        elif stanza['from'].bare == self.target_right.bare:
            target = self.target_left
        else:
            target = None

        if target:
            stanza['from'] = self.boundjid
            stanza['to'] = target
            stanza.send()

if __name__ == '__main__':
    # Ideally use optparse or argparse to get JID,
    # password, and log level.

    logging.basicConfig(level=logging.DEBUG,
                        format='%(levelname)-8s %(message)s')

    xmpp = VoIPBot(os.environ['USER'], os.environ['PASSWORD'])
    xmpp.connect()
    xmpp.process(forever=True)

    # DEBUG:
    # you can snoop on the audio/video flow by instead putting your bot in the middle
    # another way to see these would be to set up a test server with only these bots on it and turn up debug logging
    #mitm = JingleMITM(os.environ['USER'], os.environ['PASSWORD'], "vagabond@kousu.ca/Conversations.haFY", "celph@kousu.ca/Conversations.2dRf")
    #mitm.connect()
    #mitm.process(forever=True)
