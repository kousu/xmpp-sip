#!/usr/bin/env python3
"""

Usage:
  SIP_USERNAME="<you>@sip.linphone.org" SIP_PASSWORD="$(pass VoIP/sip.linphone.org | head -n 1 )" SIP_TARGET="<friend>@sip.linphone.org" ./caller.py
"""


import os
import argparse
import asyncio
import contextlib
import logging
import random

import aiosip

SIP_USERNAME=os.environ['SIP_USERNAME']
SIP_PASSWORD=os.environ['SIP_PASSWORD']
# allow overriding SIP contact address
SIP_PORT=int(os.environ.get('SIP_PORT', '5060')) # TODO: read from SIP_USERNAME ?
SIP_TARGET=os.environ['SIP_TARGET'] # TODO: make a regular argv arg?
SIP_HOST=os.environ.get('SIP_HOST', SIP_TARGET.split('@')[1])

async def login(peer):
    reg = await peer.register(
        from_details=aiosip.Contact.from_header('sip:{}'.format(SIP_USERNAME)),
        to_details=aiosip.Contact.from_header('sip:{}'.format(SIP_USERNAME)),
        contact_details=aiosip.Contact.from_header('sip:{}'.format(SIP_USERNAME)),
        password=SIP_PASSWORD
    )
    return reg

async def run_call(peer, duration):
    call = await peer.invite(
        from_details=aiosip.Contact.from_header('sip:{}'.format(SIP_USERNAME)),
        to_details=aiosip.Contact.from_header('sip:{}'.format(SIP_TARGET)),
        password=SIP_PASSWORD)
    async with call:
        async def reader():
            async for msg in call.wait_for_terminate():
                print("CALL STATUS:", msg.status_code)

            print("CALL ESTABLISHED")
            await asyncio.sleep(5)
            print("GOING AWAY...")

        with contextlib.suppress(asyncio.TimeoutError):
            await asyncio.wait_for(reader(), timeout=duration)

    print("CALL TERMINATED")


async def start(app, protocol, duration):
    peer = await app.connect(
        (SIP_HOST, SIP_PORT),
        protocol=protocol)
    session = await login(peer)
    try:

        await run_call(peer, duration)
    finally:
        await session.close()
    await app.close()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--protocol', default='udp')
    parser.add_argument('-d', '--duration', type=int, default=5)
    args = parser.parse_args()

    loop = asyncio.get_event_loop()
    app = aiosip.Application(loop=loop)

    if args.protocol == 'udp':
        loop.run_until_complete(start(app, aiosip.UDP, args.duration))
    elif args.protocol == 'tcp':
        loop.run_until_complete(start(app, aiosip.TCP, args.duration))
    else:
        raise RuntimeError("Unsupported protocol: {}".format(args.protocol))

    loop.close()


if __name__ == '__main__':
    logging.basicConfig(level=0)

    main()
