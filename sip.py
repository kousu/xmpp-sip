import os

import argparse
import asyncio
import contextlib
import logging
import random

import aiosip

sip_config = {
    'srv_host': os.environ["USER"].split("@")[-1].strip(),
    'srv_port': 5060,
    'user': os.environ["USER"].split("@")[0].strip(),
    'pwd': os.environ["PASSWORD"],
    'local_host': '0.0.0.0',
}

async def login(peer):
    await peer.register(
        from_details=aiosip.Contact.from_header('sip:{}@{}'.format(
            sip_config['user'], sip_config['srv_host']
            )),
        to_details=aiosip.Contact.from_header('sip:{}@{}'.format(
            sip_config['user'], sip_config['srv_host']
            )),
        contact_details=aiosip.Contact.from_header('sip:{}@{}'.format(
            sip_config['user'], sip_config['srv_host'],
            )),
        password=sip_config['pwd']
    )

async def run_call(peer, target, duration):
    call = await peer.invite(
        from_details=aiosip.Contact.from_header('sip:{}@{}'.format(
            sip_config['user'], sip_config['local_host'])),
        #to_details=aiosip.Contact.from_header('sip:666@{}:{}'.format(
        #    sip_config['srv_host'], sip_config['srv_port'])),
        to_details=aiosip.Contact.from_header(target),
        password=sip_config['pwd'])

    async with call:
        async def reader():
            async for msg in call.wait_for_terminate():
                print("CALL STATUS:", msg.status_code)

                if msg.status_code == 200:
                    print("CALL ESTABLISHED")
                await asyncio.sleep(5)
            print("GOING AWAY...")

        with contextlib.suppress(asyncio.TimeoutError):
            await asyncio.wait_for(reader(), timeout=duration)

    print("CALL TERMINATED")


async def start(app, protocol, duration):
    peer = await app.connect(
        (sip_config['srv_host'], sip_config['srv_port']),
        protocol=protocol,
        local_addr=(sip_config['local_host'], 0))

    session = await login(peer)
    try:
        if os.environ['TARGET']:
            await run_call(peer, os.environ['TARGET'], duration)
        else:
            print("No TARGET passed. Did you want to call someone?")
    finally:
        await session.close()
    await app.close()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--protocol', default='udp')
    parser.add_argument('-d', '--duration', type=int, default=5)
    args = parser.parse_args()

    loop = asyncio.get_event_loop()
    app = aiosip.Application(loop=loop)

    if args.protocol == 'udp':
        loop.run_until_complete(start(app, aiosip.UDP, args.duration))
    elif args.protocol == 'tcp':
        loop.run_until_complete(start(app, aiosip.TCP, args.duration))
    else:
        raise RuntimeError("Unsupported protocol: {}".format(args.protocol))

    loop.close()


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    main()
