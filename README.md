WebRTC <-> SIP Bridge
=====================

[Conversations 2.8.0](https://conversations.im) grew voice/video. It's compatible with [Movim](https://movim.eu) too, so you can do calling on your self-hosted server from almost anywhere now! That's the dream anyway.

[JMP.chat](https://jmp.chat) ((code [here](https://gitlab.com/ossguy/sgx-catapult/)))
already runs an XMPP->SMS bridge, with SIP accounts on the side giving calling to the real phone network.
The high-level goal of this project is to combine the new Conversations with that to
make a complete VoIP app in one, while maintaining the benefits of mostly self-hosted infrastructure.

## Usage

Set up a couple of test XMPP accounts [somewhere](https://compliance.conversations.im) -- there's hundreds of servers now. Go forth!
Though for this you probably want to pick one with [VoIP support](https://compliance.conversations.im/test/turn/) -- or [install your own](https://gist.github.com/iNPUTmice/a28c438d9bbf3f4a3d4c663ffaa224d9#notes-for-server-admins). It's really not that scary, email me if you want some pointers getting that going for yourself.

Install Conversations 2.8 on an Android phone (Google Play Store or F-Droid, both are good). Sign in to one of your accounts there; add the other one as a friend.


Install dependencies:

```
pip3 install -r requirements.txt
```

XMPP
----

To test receiving calls:

```
USER=bot@example.net PASSWORD=password ./bot.py 2>&1 | ./tools/logpretty
```

In Conversations you should see it come online and gain a Phone icon in the top right corner. You can press that to have it try to connect, and debug it.

To test sending calls:

```
USER=bot@example.net PASSWORD=password TARGET=you@example.net ./bot.py 2>&1 | ./tools/logpretty
```

It should ring your phone. You can decline or accept and it should proceed to the next step properly.


If you want to see more details, you can use `adb logcat`, as per [iNPUTmice's instructions](https://gist.github.com/iNPUTmice/a28c438d9bbf3f4a3d4c663ffaa224d9#notes-for-developers):

1. Enable Developer Mode
2. Turn on "Android Debugging" (i.e. `adb`)
  * you can turn on debugging over wifi.
3. Install `adb` on your computer (`apt-get install android-tools-adb`, or the equivalent for your system; ask around and feed back the tips)
4. (if using debugging over WiFi) Run `adb connect <android_ip>`
4. (if using a cable) plug in your phone with its USB cable to your computer
5. Run `adb shell` to test if adb is connected right; you will probably need to authorize the connection at the prompt on your phone.
6. `adb logcat -v time -s conver6ations` (if using Conv6ations) or `adb logcat -v time -s conversations` if not
7. Try running the bot and making some calls. You should see lots of detail about what it's doing!



This should also work with Movim, but no promises yet. And some day we'll talk meet.jit.si and Gajim into agreeing. But that's a digression.


SIP
---

At the moment, the SIP prototype is a separate program.
I don't think it will be hard to integrate.

Just logging in and waiting for incoming calls:

```
USER="you@jmp.bwapp.bwsip.io" PASSWORD="password" python3 sip.py
```

Again, to make an outgoing call pass TARGET:

```
USER="you@jmp.bwapp.bwsip.io" PASSWORD="password" TARGET="sip:test.time@sip5060.net" python3 sip.py
```

---

This is very much a work in progress, but it's a *fun* work in progress.

The idea is to eventually merge this into [sgx-catapult](https://gitlab.com/ossguy/sgx-catapult/) so that JMP contacts appear natively with a calling icon.


# Plan

### Prototype

* [x] Implement the relevant disco features to get Conversations/Movim to recognize the bot as callable
* [x] Implement XEP-0353 (multi-device ringing)
* [x] Implement [XEP-0166 Jingle](https://xmpp.org/extensions/xep-0166.html)
  * this is not yet part of slixmpp, so I need to do it from scratch
  * (okay I've checked this off but it's not really done; it works enough to start a call but it needs to be cleaned up and turned into a slixmpp plugin and upstreamed)
* [ ] Spawn a SIP connection so there's somewhere to proxy to
* [ ] Learn about
  * [ICE](https://tools.ietf.org/html/rfc8445)
  * STUN/[TURN](https://tools.ietf.org/html/rfc5766)
  * [XEP-0176 Jingle ICE-UDP](https://xmpp.org/extensions/xep-0176.html)
  * [XEP-0167 Jingle RTP](https://xmpp.org/extensions/xep-0167.html#info-active)
  * [DTLS-SRTP](https://tools.ietf.org/html/rfc5764)
    * this is what the 'fingerprint' field in the jingle sessions is for; it's labelled "urn:xmpp:jingle:apps:dtls:0" but it's actually DTLS-SRTP.
    * https://github.com/persmule/libdtlssrtp/ - based on Asterisk; unmaintained, but maybe useful?
* [ ] Figure out how to set up a DTLS-SRTP terminator (since [BandwidthAP](https://www.bandwidth.com/voice/sip-trunking/) doesn't support any kind of encryption)
  * maybe this means writing one
  * maybe it means deploying Asterisk with a dialplan that makes it proxy back and forth
  * and the terminator needs to be rigged up to listen to commands from the XMPP module.
  * maybe this means deploying [coturn](https://github.com/coturn/coturn/) -- it already does DTLS, though now DTLS-SRTP as far as I can tell
    and getting it to *terminate* DTLS-SRTP probably means modifying it.


#### DTLS-SRTP

Because BandwidthAP doesn't run any encryption, but Conversations demands it, we're going to have to install highly-targettable
and unfortunately very unobvious crypto-terminators. Basically reverse proxies, but for ICE sessions instead of HTTP.
My initial hope was just to start XMPP/Jingle on the left, SIP/SDP on the right, and [translate](https://xmpp.org/extensions/xep-0167.html),
but it's impossible because of this.

I see two approaches left:

1. Building on [eta's work](https://theta.eu.org/lx/asterisk.patch), put up an Asterisk server.
   * Configure it to proxy XMPP/Jingle/DTLS-SRTP <-> SIP/ICE/RTP;
     * eta's work so far only supports one special bot user so the prototyping work I've done so far here would need to be merged (and rewritten) into it in order
       to handle multiple devices and bidirectional calling, and it doesn't yet proxy to an external SIP trunk, though I'm told by @ossguy and @singpolyma et al. that this is commonplace matter for Asterisk,
       by means of installing the right "dialplan", so it should be doable. It would be a fairly complicated dialplan, though, since it would need to be given the Bandwidth SIP credentials for each customer and know the mapping between their XMPP accounts and their SIP accounts.
   * To hook this into JMP.chat, add special-case logic to the JMP.chat bridge that filters out/in audio/video specific messages (so, that means XEP-0353 and XEP-0166 packets).
   * The final setup for a customer with XMPP account customer@example.net, paying for an account with phone number +1-555-123-4567, would run:
     * Rings: `{[xmpp:customer@example.net]} <--[XMPP/Jingle]--> {xmpp:cheogram.com} <--[XMPP/Jingle]--> (( {xmpp:customer~40example.net@asterisk.jmp.chat} <-(internal process communication)-> {sip:customer~40example.net@asterisk.jmp.chat} )) <--[SIP/ICE]--> {sip:customer~40example.net@jmp.bwapp.bwsip.io}`
     * Audio: `{[smartphone]} <--[UDP/DTLS-SRTP]--> {udp://asterisk.jmp.chat:5060} <--[UDP/RTP]--> {udp://jmp.bwapp.bwsip.io} <--[internal BandwidthAP infrastructure]--> PSTN`
   * Pro:
     * The Cheogram modifications are extremely easy: just add a small packet filter (you can use [XPath](https://en.wikipedia.org/wiki/XPath)) that changes the
     * from/to addresses on audio/visual packets and forwards them on: a ring from `xmpp:customer@example.net` to `xmpp:+15559999999@cheogram.com` becomes a ring from `xmpp:customer~40@cheogram.com` to `xmpp:+15559999999@asterisk.jmp.chat`; and there shouldn't be much code needed on the asterisk side.
     * The work is (largely?) already done? Certainly if eta and I put our heads together we can port the more frontendey parts from this repo in their asterisk patch.
     * Handing out the correct DTLS-SRTP fingerprints is simpler because they're just contained in the XMPP stanzas and the XMPP server that has to parse them is also the SIP server that has to open up a DTLS-SRTP endpoint.
   * Con:
     * An extra layer of complexity
     * This locks you into only having a single server doing all the media proxying, which seems very heavy and fragile.
       * unless you add yet another layer of complexity by having the central Asterisk server coordinate; maybe Asterisk can set up a replicated-cluster?
2. Use [coturn](https://github.com/coturn/coturn/).
   * coturn has DTLS support, but I'm unclear if it has DTLS-SRTP, and moreover even if it did I am unclear if that would help since it probably isn't designed to be used as an (de/)encryption proxy. Nevertheless it has an appeal. With this, the regular JMP.chat bridge itself would be logged in to SIP, and it would arrange to tell the customer's XMPP client and the SIP carrier to meet in the middle as at a TURN proxy, but the former using encryption and the latter not, with neither needing to be the wiser.
   * * To hook this into JMP.chat, all you need to do is add the coturn server to prosody, but then make sure to tweak the [part that does Jingle-SDP translation](https://xmpp.org/extensions/xep-0167.html) subtly to map DTLS-SRTP on the XMPP side into unencrypted RTP on the SIP side. 
   * The final design would look like:
     * Rings: `{xmpp:customer@example.net} <--[XMPP/Jingle]--> {xmpp:cheogram.com} <--[SIP/ICE]--> {sip:customer~40example.net@jmp.bwapp.bwsip.io}`
     * Audio: `{[smartphone]} <--[DTLS-SRTP]--> {udp://calls.jmp.chat:5060} <--[RTP]--> {udp://jmp.bwapp.bwsip.io:5060} <--[internal BandwidthAP infrastructure]--> PSTN`
   * Pro:
     * The signalling path is simpler.
       * There is no need to store the SIP credentials in multiple servers.
     * coturn is already designed to be a proxy, so this should be an easy fit, you'd think.
     * Easy scaling/load balancing: coturn doesn't need to coordinate with XMPP the way Asterisk would;
        because it uses [time limited TURN credentials](https://xmpp.org/extensions/xep-0215.html) based on a preshared HMAC secret to authenticate clients.
        So you can just put up a bunch of copies and give them all the same name (`turn.jmp.chat`? `calls.cheogram.com`?).
        You can even do easy DNS-based geographic routing, then, to minimize latency all around the world.
   * Con:
     * More work; **requires patching coturn**. And I have FUD about whether coturn would look kindly on intentionally weakening its security.


#### Generic Bridging

A generic Jingle<->SIP bridge.

The idea is: this would run as its own server (say on `bridge.tel`) and expose two protocols, xmpp: and sip:.
Rings (XMPP Jingle stanzas) sent `from='xmpp:juliet@xmpp.net' to='xmpp:romeo=40sip.example.net@bridge.tel'` get rewritten to
SIP INVITEs `from='sip:juliet=40xmpp.net@bridge.tel' to='sip:romeo@sip.net'` and sent on.

The reverse, SIP INVITEs `from='sip:romeo@sip.net' to='sip:juliet=40xmpp.net@bridge.tel'` are rewritten
to XMPP Jingle stanzas `from='xmpp:romeo=40sip.net@bridge.tel' to='xmpp:juliet@xmpp.net'`.

And all the content is translated too, with [XEP-0167](https://xmpp.org/extensions/xep-0167.html) as a guide.

For actual deployment, the sip addresses would become like `sip:+15551234567@jmp.bwapp.bwsip.io`.
Jingle->SIP would cover outgoing calls, and SIP->Jingle would cover incoming.

None of this addresses the DTLS-SRTP termination conundrum, above. That (probably?) requires slightly hacking the bridge to lie about DTLS-SRTP to..certain endpoints? maybe? or maybe we need yet another bridge.

Note: incoming messages to the bridge that don't follow the format get dropped (or, they could even *try* to connect, they just won't go anywhere)


To control abuse, add an XMPP/SIP/IP firewall that limits what servers can access the bridge.

This can be done without any special per-account sign in or anything; sip doesn't necessarily or even usually need you to be online anywhere, and actually neither does xmpp)


### JMP.chat

* [ ] Set up a test lab instance of jmp.chat
  * I expect this to take me some time to figure out all the parts since it's balkanized; and also highly-dependent on a corporate API I don't have access to.
  * Recommendation from @ossguy:
    * https://gitlab.com/soprani.ca/sgx-bwmsgsv2 is the newest code
    * it can be run without actually having access to the corporate API;
    * Use [CallWithUs](http://www.callwithus.com/) to get a cheap PSTN-connected SIP endpoint to test with,
      and then we can productionize it by swapping that SIP endpoint for Bandwidth's endpoint; Bandwidth doesn't actually need per-user accounts; rather, incoming calls come from sip:+1555123456789@<some-ip-address>, and outgoing calls can just be made.
    * https://gitlab.com/ossguy/jmp-fwdcalls is "call control", such as it is so far (which is not much)
* [ ] Port the prototype from python to ruby
  * [ ] Disco features
  * [ ] XEP-0353
  * [ ] Jingle
    * I think this is more complete in ruby than in python; at least, jmp.chat already uses jingle for MMS messages sometimes
  * [ ] Spawning a direct SIP connection
* [ ] Deploy the DTLS-SRTP proxy? And hook it up to XMPP, of course.

### Then: scaling?

Adding the DTLS-SRTP proxy in the mix is going to be a giant headache, both in terms of load-balancing and also in privacy concerns.
But it's necessary until Bandwidth supports encryption natively. Of course, it seems a bit silly to encrypt the complicated internet half of the circuit and not the other half, but Conversations (rightly) insists on encryption and so to get this to work it's gotta be in the mix somewhere.

* some numbers: [Discord runs](https://blog.discord.com/how-discord-handles-two-and-half-million-concurrent-voice-users-using-webrtc-ce01c3187429) 850 servers in 30 data centers, handling 2.6e6 connections at 220Gb/s in total. They run on Google's datacentres. So that's ~3000 connections per server, and 32MB/s traffic per server. So like, *a lot*, but it's not *crazy a lot*, my home connection can do 5MB/s.

# Tools

* WebRTC/RTP/ICE tester: https://webrtc.github.io/samples/src/content/peerconnection/trickle-ice/ ( recommended by https://prosody.im/doc/coturn )


# Literature review


Jingle

* https://gitlab.com/ossguy/sgx-catapult/-/blob/26602997e9ab9d6833d65ace5606f4d43942318e/sgx-catapult.rb#L461
  * the existing jmp.chat Jingle codebase, which is a useful reference.

VoIP

- List of implementations: https://en.wikipedia.org/wiki/WebRTC_Gateway
- https://sylkserver.com/ / https://webrtc.sipthor.net/
- https://github.com/havfo/WEBRTC-to-SIP
- https://webrtc.ventures/2018/03/webrtc-sip-the-demo/
- https://www.html5rocks.com/en/tutorials/webrtc/infrastructure/

- https://www.onsip.com/voip-resources/voip-fundamentals/webrtc-sip-and-html5-brief-introduction
- Twilio even: https://www.twilio.com/client/sip-to-webrtc
- https://sipjs.com
- https://webrtcglossary.com/rtcp-mux/

- http://webrtc-security.github.io/
- https://jitsi.org/security/ - what jitsi has to say about the security of WebRTC/DTLS-SRTP.


https://wiki.asterisk.org/wiki/display/AST/Asterisk+WebRTC+Support
https://xmpp.org/extensions/xep-0167.html#srtp
https://wiki.asterisk.org/wiki/display/AST/Secure+Calling+Tutorial#SecureCallingTutorial-ConfiguringaTLS-enabledSIPclienttotalktoAsterisk
https://stackoverflow.com/questions/31421909/difference-between-dtls-srtp-and-srtp-packets-send-over-dtls-connections#31432779
https://stackoverflow.com/questions/27584498/what-is-the-minimal-sdp-answer-to-receive-webrtc-audio-and-video?rq=1

https://github.com/mweibel/sdpToJingle
https://xmpp.org/extensions/inbox/jingle-sdp.html
https://xmpp.org/extensions/xep-0176.html
https://xmpp.org/extensions/xep-0167.html


https://gultsch.de/files/av-calls.pdf
https://www.youtube.com/watch?v=v_riuu4XU6Q&feature=youtu.be&t=3040
- [eta](https://theta.eu.org/) got calling working with Conversations + Asterisk + jmp.chat
  though it required [patching Asterisk](https://theta.eu.org/lx/asterisk.patch)
- demo at xmpp:asterisk@theta.eu.org (caveat: only an echo test right now, and you have to trigger it with the word "testme" instead of using GUI buttons.)


SIP:

* [pjsua](https://www.pjsip.org/docs/book-latest/html/intro_pjsua2.html#building-python-and-java-swig-modules)
  * this looks very finished, and it's what Asterisk is built on, but also SWIG sounds painful to use.
  * https://stackoverflow.com/questions/20195542/python-how-to-get-the-import-pjsua-giving-no-module-named-pjsua
  * http://trac.pjsip.org/repos/browser/pjproject/trunk/pjsip-apps/src/python/samples/registration.py
  * http://trac.pjsip.org/repos/browser/pjproject/trunk/pjsip-apps/src/python/samples/call.py
  * unfortunately, the SWIG bindings don't seem to be packaged in Debian, and there's obviously no pip package to use.
* [rtclite](http://39peers.net/) - a complete alternate RTC/RTP/SIP stack https://github.com/theintencity/rtclite. in python.
  * unfortunately, only python2 is maintained right now(??), it's not packaged to pypi, it's possibly undergoing some rearchitecture right now,
    and it's based on gevent which makes it incompatible or at least awkward to use with asyncio.
* [aiosip](https://github.com/Eyepea/aiosip/) - asyncio-based SIP implementation; newer; doesn't have stability yet though.

* https://github.com/vodik/sipster
* https://pypi.org/project/linphone/ is apparently in pypi??
* what https://pypi.org/project/PyAliSip/
* https://pypi.org/project/Katari/ this actually looks super well developed, but probably not used by anyone

* https://pypi.org/project/sipvicious/ a different flavor of SIP library :P
* https://most-voip.readthedocs.io/en/latest/python_docs/tutorial/voip_tutorial_1.html this looks unfinished but promising?

